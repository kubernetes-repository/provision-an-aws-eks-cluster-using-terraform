# About the Project
## The project to design a CICD pipeline to auto provision the AWS EKS cluster using Terraform
*  Terraform configuration file (*.tf) is stored on GitLab Repo
*  Terraform uses state files to store details about the infrastructure configuration. In this project, I use GitLab as Terraform HTTP backend to securely store the state file. Note that, GitLab supports state locking to to prevents others from acquiring the lock and potentially corrupting your state

  ![image](/uploads/d41b5c83e7205c281ecaf4e59e48884f/image.png)

## Prerequisite
* To interact with your AWS account, the GitLab CI/CD pipelines require both AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to be defined in your GitLab project settings. You can do this under Settings > CI/CD > Variables.

## The CICD Pipeline
* CI/CD Pipeline: **GitLab Repo** ----[trigger]---> **Gitlab Runner(default)**---[pull]-->**Terraform(image)**---[deploy]---> **AWS Cloud**

  ![image](/uploads/0d6488ff265823b9b9a7428b3589973e/image.png)

* Pipeline stages
  - Validate: validates the Terraform configuration files pulled from GitLab repo
  - Execute plan: creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure
  - Create cluster: executes the actions proposed in the plan stage to create an EKS cluster
  - Clean up: destroy all AWS resources managed by the Terraform configuration

  ![image](/uploads/95de19ba73d20082acb2ea73730e4595/image.png)

## Note
* The backend.tf file must be stored in the same directory with other Terraform files (*.tf) which are pulled from Github. Otherwise, you will lose the terrafrom state file after running "terraform apply".  
